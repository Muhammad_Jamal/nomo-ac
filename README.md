# Nomo test automation framework
This repo contains tests and documents for:
* Nomo homepage (http://www.nomobank.com/)
* Property Finance page (http://www.nomobank.com/rental-property-finance/)

## Tools & Programming Languages
* Cypress
* Javascript

## Tests
### Prerequisites
* You need to install node https://nodejs.org/en/download/
* This document helps you to know more about the system requirements: https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements

### Installation
* In your favorite IDE (ex: vs code) create a new folder
* clone the repository to local folder "https://gitlab.com/Muhammad_Jamal/nomo-ac.git"
* run "npm install" to download dependencies

### Frontend Tests & Documents
* Tests: cypress/e2e/nomo-ui-tests
* Test cases document:
* you can check this video for the frontend tests when they are running: https://drive.google.com/file/d/1I8dB8fSH119r8e_TD9mVl-BX5FxyhW3K/view?usp=sharing

### NPM Scripts to run the backend and frontend tests

| Script                 | Usage           |
| -------------          |:-------------:|
| pretest                | create report folders|
| run-ui-headless                | run UI tests in headless mode|
| posttest               | combine and generate html report|
| clean:reports          | delete reports folder|
| run-tests              | run frontend tests in watch mode|

### Reporting
* Mochawesome report is used to generate html/json reports for tests by running scripts "npm run pretest => npm run run-tests-headless => npm run posttest" 
* Reports can be found in cypress/reports/mochareports

### Pipeline
#### Platform: GitLab
-Trigger methods:
  * Automatically with every new commit
  * Manually from the start pipeline button in GitLab (https://gitlab.com/Muhammad_Jamal/nomo-ac/-/pipelines)

#### What could be the next steps to your project
  * Add new test cases to cover the remaining features
  * Add visual testing

### Authors and acknowledgment
Muhammad Jamal