/// <reference types = "cypress" />

describe('nomo automation challenge', () => {
  context('nomo homepage', () => {
    beforeEach(() => {
      cy.visit('/')

      cy.get('.jss14 > .MuiTypography-root')
        .as('heroTitle')

      cy.contains('Property Finance')
        .as('propertyFinanceMenu')

      cy.contains('Rental Property')
        .as('rentalPropertyBtn')
    })

    it('verify user can navigate to nomo', () => {
      cy.url()
        .should('eq', 'https://www.nomobank.com/')

      cy.get('@heroTitle')
        .should('be.visible')
        .and('contain', 'Introducing multi-currency accounts')
    })

    it('verify user can navigate to Rental Property page', () => {
      cy.get('@propertyFinanceMenu')
        .click()

      cy.get('@rentalPropertyBtn')
        .click()

      cy.url()
        .should('eq', 'https://www.nomobank.com/rental-property-finance')

      cy.contains('Invest in UK rental property')
        .should('be.visible')
    })
  })

  context('rental property page', () => {
    beforeEach(() => {
      cy.visit('/rental-property-finance')

      cy.get('iframe.mc-embedded_Iframe__27PR_')
        .as('financeCalculator')
    })

    it('verify property finance is calculated correctly', () => {
      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-property"]')
        .clear()

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-property"]')
        .type('1000000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-rental-income"]')
        .should('be.visible')
        .type('1000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="down-payment"]')
        .should('be.visible')
        .type('500000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('.MuiSelect-select')
        .click()

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('li.MuiMenuItem-root:nth-child(1)')
        .click()

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('.mc-results_MCResultsValueOfFinance__CBt1j')
        .should('contain', '92,915 GBP')

      cy.get("@financeCalculator")
        .iframeDirect().find('.mc-results_MCResultsMonthlyCosts__pRvOw')
        .should('contain', '458 GBP')

    })

    it('verify error message will appear in case down payment < 30% property value', () => {
      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-property"]')
        .clear()

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-property"]')
        .type('500000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-rental-income"]')
        .should('be.visible')
        .type('1000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="down-payment"]')
        .should('be.visible')
        .type('140000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('.mc-form_ValidationMsgError__JNryE')
        .should('be.visible')
        .should('contain', 'The down payment needs to be higher')
    })

    it('verify error message will appear in case finance amount is < 100,000 GBP', () => {
      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-property"]')
        .clear()

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-property"]')
        .type('50000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="estimate-rental-income"]')
        .should('be.visible')
        .type('500')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('[name="down-payment"]')
        .should('be.visible')
        .type('25000')

      cy.get("@financeCalculator")
        .iframeDirect()
        .find('.mc-form_ValidationMsgError__JNryE')
        .should('be.visible')
        .should('contain', 'The minimum finance amount  is 100,000 GBP')
    })
  })
})