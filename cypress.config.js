const { defineConfig } = require("cypress");

module.exports = defineConfig({
  // Defining viewport  
  viewportHeight: 754,
  viewportWidth: 1536,

  // Disable same-origin policy
  chromeWebSecurity: false,

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'http://www.nomobank.com/',
    reporter: "mochawesome",
    screenshotsFolder: "cypress/reports/screenshots",
    reporterOptions: {
      reportDir: "cypress/reports/mocha",
      reportName: "myTestProfile",
      reportTitle: "Nomo Report",
      reportPageTitle: "Nomo Report",
      jsonReport: true,
      overwrite: false,
      screenshotOnRunFailure: true
    },
    env: {
    }
  },
});
